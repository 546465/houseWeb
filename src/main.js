// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import footerView from './components/public/foot';
import headerView from './components/public/header';
import headerView1 from './components/public/header1';
import 'vue2-toast/lib/toast.css';
import Toast from 'vue2-toast';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import axios from 'axios';
import {post, get, patch, put} from './http/http';
import router from './router';
import infiniteScroll from 'vue-infinite-scroll';
import store from './store/store'




Vue.use(infiniteScroll);
Vue.use(VueAwesomeSwiper);
Vue.use(Toast);
Vue.component('footerView', footerView);
Vue.component('headerView', headerView);
Vue.component('headerView1', headerView1);
Vue.config.productionTip = false;
//定义全局变量
Vue.prototype.$post = post;
Vue.prototype.$get = get;
Vue.prototype.$patch = patch;
Vue.prototype.$put = put;
Vue.use(require('vue-wechat-title'));

/*let { href, protocol, host, hash } = window.location;
hash = hash || '#/';
let url = protocol + '//' + host + '/' + hash;
if (url !== href) window.location.replace(url);*/

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: {App},
    template: '<App/>'
});
