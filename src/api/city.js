//城市接口
import {get} from './../http/http'

const city={
    //城市列表
    list:function(){
        return get('/api/city',{})
    },
    //得到默认城市
    defaultCity:function(){
         return get('/api/defaultCity',{})   
    },
    houseList:function(){
        return  get('/api/workshop/1',{})
    },
    city:function(id){
         return get('/api/city/'+id,{})   
    }
    //
}

export default city
