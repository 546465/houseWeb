import city from '../../api/city'
export default{
    state:{
        city_title:false,
        city_id:false
    },
    mutations: {
        city (state,city_id) {//得到当前城市

          city.city(city_id).then(response=>{
			if(response.code==0){
                state.city_title=response.data.title,
                state.city_id=response.data.id
			}else{
				city.defaultCity().then(response => {
                    state.city_title=response.data.title,
                    state.city_id=response.data.id
				});
			}	
		    }
		)

           
        },
        cityId(state,v){
            state.city_id = v
        },
        defautCity(state){
            city.defaultCity().then(response => {
                state.city_title=response.data.title,
                state.city_id=response.data.id
            });
        }
        
      },
      actions: {
        city(context,payload){
            context.commit('city',payload.cityId)
        },
        defaultCity(context){
            context.commit('defautCity')    
        }
      }
}