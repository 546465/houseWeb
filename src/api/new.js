
import {get} from './../http/http'
//首页api封装
const news={
    list:function(){
        return get('api/news',{});
    },
    detail:function(id){
        return get('api/news/'+id,{});    
    }

}
export default news