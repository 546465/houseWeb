import Vue from 'vue';
import Toast from 'vue2-toast';
import Router from 'vue-router';
import website from '../api/website';
import cityGet from '../api/city';
import store from './../store/store'


Vue.use(Router);
Vue.use(Toast);

const data= resolve =>require(['../components/data'],resolve);
const Index = resolve => require(['../components/index'], resolve);
const houseList = resolve => require(['../components/houseList'], resolve);
const cityList = resolve => require(['../components/cityList'], resolve);
const parkList = resolve => require(['../components/parkList'], resolve);
const park = resolve => require(['../components/park'], resolve);
const detail = resolve => require(['../components/detail'], resolve);
const baidu = resolve => require(['../components/baidu'], resolve);
const news = resolve => require(['../components/news'], resolve);
const city = resolve => require(['../api/city'], resolve);
const base = resolve => require(['../components/base'], resolve);
const content = resolve => require(['../components/content'], resolve);
const login = resolve => require(['../components/login'], resolve);
const user = resolve => require(['../components/user'], resolve);
const userRead = resolve => require(['../components/userRead'], resolve);
const userFocus = resolve => require(['../components/userFocus'], resolve);
const landList = resolve =>require(['../components/landList'],resolve);//园区
const landDetail= resolve =>require(['../components/landDetail'],resolve);
const buildList = resolve =>require(['../components/buildList'],resolve);
const buildDetail= resolve =>require(['../components/buildDetail'],resolve);
const count= resolve =>require(['../components/count'],resolve);




const router = new Router({
    mode: 'history',

    routes: [
        {
            path: '/',
            name: 'index',
            component: Index
        },

		{
            path: '/houseList/:type/:city',
            name: 'houseList1',
            component: houseList
        },

        {
            path: '/houseList/:type',
            name: 'houseList',
            component: houseList
        },
        {
            path: '/cityList',
            name: 'cityList',
            component: cityList
        },
        {
            path: '/detail/:id',
            name: 'detail',
            component: detail
		},
		{
			path:'/landList',
			name:'landList',
			component:landList	 //土地
		},
        {
            path:'/data',
            name:'data',
            component:data   //大数据
        },
		{
			path:'/landDetail/:id',
			name:'landDetail',
			component:landDetail //土地详情
		},
        {
            path:'/buildList',
            name:'buildList',
            component:buildList   //写字楼
        },
        {
            path:'/buildDetail/:id',
            name:'buildDetail',
            component:buildDetail //写字楼详情
        },
        {
            path: '/park/:id',
            name: 'park',
            component: park//园区详情
		},
		{
			path: '/parkList',
			name:'parkList',
			component:parkList //园区列表
		},
	
        {
            path: '/baidu/:city',
            name: 'baidu',
            component: baidu
        },
        {
            path: '/news/detail/:id',
            name: 'news',
            component: news
        },
        {
            path: '/base/:park_id/:id',
            name: 'base',
            component: base
        },
        {
            path: '/content',
            name: 'content',
            component: content
        },
        {
            path:'/login',
            name:'login',
            component:login
        },
        {
            path:'/user',
            name:'user',
            component:user
        },
        {
            path:'/count',
            name:'count',
            component:count
        },
        {
            path:'/userRead',
            name:'userRead',
            meta:{
                requireAuth:true,
            },
            component:userRead
        },
        {
            path:'/userFocus',
            name:'userFocus',
            meta:{
                requireAuth:true,
            },
            component:userFocus
		},
		{
            path: '/:city',
            name: 'index1',
            component: Index
		},
        {
			path: '/parkList/:city',
			name:'parkList1',
			component:parkList //园区列表
        },
        {
            path:'/buildList/:city',
            name:'buildList1',
            component:buildList   //写字楼
        },
        {
			path:'/landList/:city',
			name:'landList1',
			component:landList	 //土地
		},
       
		
    ]
});
router.beforeEach((to, from, next) => {

    Vue.prototype.$loading('数据加载中');

    let cityId;
    if(to.params.city!=undefined){
        cityId = to.params.city
    }else{
         cityId = localStorage.getItem('id');
    }

	if(to.params.city!=undefined){
		store.dispatch('city',{cityId:cityId})
		//store.commit('city',cityId)
	}
	if(!store.state.city.city_id&&to.params.city==undefined){
		store.dispatch('defaultCity')
	}
	//默认城市
	

    website.detail().then(res => {
        document.title = res.data.title;
        document.querySelector('meta[name="keywords"]').setAttribute('content', res.data.key);
        document.querySelector('meta[name="description"]').setAttribute('content', res.data.desc);
    });

    let token = window.localStorage.getItem('token')
    if(to.matched.some(record => record.meta.requireAuth) && (!token || token === null)){
        next({
            path:'/login'
        })
    }else{
        next();
    }

    // next();
});
router.afterEach((to, from) => {
    setTimeout(function () {
        Vue.prototype.$loading.close();
    }, 500)
});
// router.beforeEach((to, from, next) => {
//   if (to.meta.requireAuth) {         //如果需要跳转 ，往下走（1）
//     if (true) {            //判断是否登录过，如果有登陆过，说明有token,或者token未过期，可以跳过登录（2）
//       if (to.path === '/login') {    //判断下一个路由是否为要验证的路由（3）
//         next('/index');         // 如果是直接跳到首页，
//       } else {             //如果该路由不需要验证，那么直接往后走
//         next();
//       }
//     } else {
//       console.log('没有');      //如果没有登陆过，或者token 过期， 那么跳转到登录页
//       next('/login');
//     }
//   } else {                           //不需要跳转，直接往下走
//     next();
//   }
// });

export default router;
